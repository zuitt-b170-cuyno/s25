db.fruits.insertMany([
    {
        name: "apple",
        color: "red",
        stock: 300,
        price: 30,
        supplier_id: "1",
        onSale: false,
        origin: ["Japan", "Korea"]
    },
    {
        name: "mango",
        color: "yellow",
        stock: 400,
        price: 40,
        supplier_id: "2",
        onSale: true,
        origin: ["China", "USA"]
    },
    {
        name: "banana",
        color: "yellow",
        stock: 500,
        price: 20,
        supplier_id: "3",
        onSale: true,
        origin: ["Philippines", "Japan"]
    },
    {
        name: "orange",
        color: "orange",
        stock: 300,
        price: 30,
        supplier_id: "2",
        onSale: true,
        origin: ["China", "USA"]
    },
    {
        name: "kiwi",
        color: "green",
        stock: 300,
        price: 30,
        supplier_id: "2",
        onSale: false,
        origin: ["China", "USA"]
    }
])

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: { _id: 0 } },
    { $sort: { total: 1} }
])

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: { _id: 0 } },
    { $sort: { total: -1} }
])

db.fruits.updateOne(
    { supplier_id: "3" },
    {
        $set: {
            supplier_id: "2"
        }
    } 
)

db.fruits.aggregate([
    { $unwind: "$origin" },
    { $match: { origin: "China" } },
    { $group: { _id: "$origin", kinds: { $sum: 1 } } }
])

var owner = ObjectId()
db.owners.insert(
    {
        _id: owner, 
        name: "John Smith",
        contact: "09194249407"
    }
) 

db.suppliers.insert(
    {
        name: "ABC Fruit",
        contact: "09194249407",
        owner_id: ObjectId("62557422b7c91dc50c7227d1")
    }
)

db.suppliers.insert(
    {
        name: "DEF Fruit",
        contact: "09194249407",
        address: [
            {
                street: "123 San Jose Street",
                city: "Manila"
            },
            {
                street: "367 Gil Puyat Street",
                city: "Makati"
            }
        ]
    }
)

var branch1 = ObjectId(), branch2 = ObjectId()

db.suppliers.insert(
    {
        name: "GHI Fruit",
        contact: "09194249407",
        address: [
            branch1, 
            branch2
        ]
    }
)

db.branches.insertMany([
    {
        _id: ObjectId("625579d5b7c91dc50c7227d4"),
        name: "BF Homes",
        address: "123 Arcadio Santos Street",
        city: "Paranaque",
        supplier_id: ObjectId("625579d5b7c91dc50c7227d6")
    },
    {
        _id: ObjectId("625579d5b7c91dc50c7227d5"),
        name: "BF Homes",
        address: "123 Arcadio Santos Street",
        city: "Paranaque",
        supplier_id: ObjectId("625579d5b7c91dc50c7227d6")
    }
])