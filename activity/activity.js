// Get the total number of fruits (stocks) per fruit on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$name", fruitsOnSale: { $sum: "$stock" } } },
])

// Sir's improvement. Get the number of fruits on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "fruitsOnSale" },
])

// Get the fruits (stocks) with enough stock
db.fruits.aggregate([
    { $match: { stock: { $gt: 20 } } },
    { $group: { _id: "$name", enoughStock: { $sum: "$stock" } } },
])

// Sir's improvement. Get the number of fruits of enough stock
db.fruits.aggregate([
    { $match: { stock: { $gt: 20 } } },
    { $count: "enoughStock" },
])

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } },
])

db.fruits.aggregate([
    { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } },
])

db.fruits.aggregate([
    { $group: { _id: "$supplier_id", min_price: { $min: "$price" } } },
])